﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;
using Funda.Data.Models;
using Refit;

namespace Funda.Data
{
    public class FundaRepository
    {
        /// <summary>
        /// Maximum number of objects returned from a query. 
        /// </summary>
        public const int MaxPageSize = 25;

        public FundaRepository(string baseUrl, string apiKey)
        {
            BaseUrl = baseUrl ?? throw new ArgumentNullException(nameof(baseUrl));
            ApiKey = apiKey ?? throw new ArgumentNullException(nameof(apiKey));
            Service = new Lazy<IFundaService>(() => RestService.For<IFundaService>(baseUrl));
        }

        private string BaseUrl { get; }
        private string ApiKey { get; }

        private Lazy<IFundaService> Service { get; } // TODO add Unity dependency injection


        /// <summary>
        /// Fetches all of the objects of the given type for the given query
        /// </summary>
        /// <param name="type">The type of objects to query</param>
        /// <param name="query">The query to filter the objects by</param>
        /// <param name="pageSize">The number of objects to fetch per call to the server.</param>
        /// <returns></returns>
        public async Task<List<FundaObject>> GetAllObjectsAsync(string type, string query, int pageSize = MaxPageSize)
        {
            if (pageSize <= 0 || pageSize > MaxPageSize)
                throw new ArgumentOutOfRangeException(nameof(pageSize), $"{nameof(pageSize)} must be greater than 0 and not larger than {nameof(MaxPageSize)} ({MaxPageSize})");

            var service = Service.Value;

            // Get the first page 
            var response = await service.GetOfferingsAsync(ApiKey, query, type, page: 1, pageSize).ConfigureAwait(false);

            var objects = response.Objects;
            objects.Capacity = response.TotalObjectCount; // Reserve space for the rest of the data to prevent needless allocations and copying

            // Get the remaining pages

            while (response.Paging.CurrentPage < response.Paging.PageCount) // Stop when the last retrieved page == PageCount
            {
                try
                {
                    Debug.WriteLine($"Fetching page {response.Paging.CurrentPage + 1}");
                    await Task.Delay(TimeSpan.FromMilliseconds(200)); // Delay a while to avoid rate limiting
                    response = await service.GetOfferingsAsync(ApiKey, query, type, response.Paging.CurrentPage + 1, pageSize).ConfigureAwait(false);
                }
                catch (ApiException ex) when (ex.StatusCode == HttpStatusCode.Unauthorized) // Happens when the server is rate limiting us.
                {
                    // Delay for a longer while to get out of rate limiting and try again
                    Debug.WriteLine("Rate limited!");
                    await Task.Delay(TimeSpan.FromSeconds(30));
                    continue;
                }

                objects.AddRange(response.Objects);
            }

            Debug.Assert(objects.Count == response.TotalObjectCount);

            return objects;
        }
    }
}
