﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Funda.Data.Models;
using Refit;

namespace Funda.Data
{
    interface IFundaService
    {
        [Get("/Aanbod.svc/json/{apiKey}")] // E.g. http://partnerapi.funda.nl/feeds/Aanbod.svc/json/[key]/?type=koop&zo=/amsterdam/tuin/&page=1&pagesize=5
        Task<OfferingResponse> GetOfferingsAsync(string apiKey, [AliasAs("zo")] string query, string type, int page, int pageSize);
    }
}
