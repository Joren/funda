﻿using Newtonsoft.Json;

namespace Funda.Data.Models
{
    /// <summary>
    /// Paging metadata for an offering query.
    /// 
    /// Note only a subset of the available properties has been implemented. 
    /// </summary>
    public class PagingData
    {
        /// <summary>
        /// Total number of pages for the query.
        /// </summary>
        [JsonProperty("AantalPaginas")]
        public int PageCount { get; set; }

        /// <summary>
        /// The current page (up to PageCount) of the query.
        /// </summary>
        [JsonProperty("HuidigePagina")]
        public int CurrentPage { get; set; } // Numbered starting at 1.
    }
}