﻿using System;

namespace Funda.Data.Models
{
    public readonly struct Broker : IEquatable<Broker>
    {
        public Broker(int id, string name)
        {
            Id = id;
            Name = name ?? throw new ArgumentNullException(nameof(name));
        }

        public int Id { get; }

        public string Name { get; }


        public override bool Equals(object obj) => obj is Broker broker && Equals(broker);

        public bool Equals(Broker other) => Id == other.Id;

        public override int GetHashCode() => 2108858624 + Id.GetHashCode();

        public override string ToString() => $"{Id}: {Name}";
    }
}
