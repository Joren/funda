﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Funda.Data.Models
{
    /// <summary>
    /// Model for an object on the Funda site.
    /// 
    /// Note only a subset of the available properties has been implemented. 
    /// </summary>
    public class FundaObject // Not named simply Object to avoid confusion with System.Object
    {
        /// <summary>
        /// Unique id for the broker of this object
        /// </summary>
        [JsonProperty("MakelaarId")]
        public int BrokerId { get; set; }

        /// <summary>
        /// Name of the broker of this object
        /// </summary>
        [JsonProperty("MakelaarNaam"), JsonRequired]
        public string BrokerName { get; set; }


        [JsonIgnore]
        public Broker Broker => new Broker(BrokerId, BrokerName); 
    }
}
