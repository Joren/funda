﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Funda.Data.Models
{
    /// <summary>
    /// Response model of the `Aanbod` (offering) service.
    /// 
    /// Note only a subset of the available properties has been implemented.
    /// </summary>
    public class OfferingResponse
    {
        /// <summary>
        /// Object models for the current page of the request
        /// </summary>
        [JsonRequired]
        public List<FundaObject> Objects { get; set; }

        /// <summary>
        /// Paging metadata for the request
        /// </summary>
        [JsonRequired]
        public PagingData Paging { get; set; }

        /// <summary>
        /// Total count of objects the offering service has available for the query, including all pages.
        /// </summary>
        [JsonProperty("TotaalAantalObjecten")]
        public int TotalObjectCount { get; set; }
    }
}
