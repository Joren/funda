﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Funda.Data;

namespace Funda
{
    class Program
    {
        private const string ApiKey = "ac1b0b1572524640a0ecc54de453ea9f";
        private const string BaseUrl = "http://partnerapi.funda.nl/feeds";

        // Note: async Main to get into an async context as early as possible
        static async Task Main(string[] args)
        {
            var repository = new FundaRepository(BaseUrl, ApiKey);

            string objectType = "koop";
            int limit = 10;

            Console.WriteLine($"Fetching top {limit} brokers in Amsterdam: ...\n");
            await PrintTopBrokersAsync(repository, objectType, "/amsterdam/", limit);

            Console.WriteLine($"\nFetching top {limit} brokers in Amsterdam considering only objects with a garden: ...\n");
            await PrintTopBrokersAsync(repository, objectType, "/amsterdam/tuin/", limit);

            Console.WriteLine("\nDone.");
            Console.ReadKey();
        }

        private static async Task PrintTopBrokersAsync(FundaRepository repository, string objectType, string query, int limit)
        {
            var objects = await repository.GetAllObjectsAsync(objectType, query); // TODO create a query builder rather than building query strings ('/amsterdam/tuin/') manually

            // Group objects by broker and order the brokers by the number of objects
            var brokerQuery = from x in objects
                              group x by x.Broker into g
                              let broker = g.Key
                              let count = g.Count()
                              orderby count descending
                              select (broker, count);

            // Print the top `limit` brokers by number of objects.
            int i = 1;
            foreach (var (broker, count) in brokerQuery.Take(limit))
                Console.WriteLine($"{i++,3}. {count,4} objects: {broker.Name}");
        }
    }
}
